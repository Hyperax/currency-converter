
# Currency Converter

The repository contains an implementation of test task provided by Revolut Company

## Getting Started

To comfortable work with the project it's preferred to use Android Studio 3.3+

### Prerequisites

Application require Android SDK ≥ 21

## Features

* Respecting of decimal places of a currency 
* Validating of user input
* Automatically and manually retrying connection to get actual currency rates
* Orientation changes support

## Built With

* [Android gradle plugin 3.3.0](https://developer.android.com/studio/releases/gradle-plugin#revisions)
* [Kotlin 1.3](https://github.com/JetBrains/kotlin)
* [Android Support Library 28](https://developer.android.com/topic/libraries/support-library/packages)
* [Dagger 2](https://github.com/google/dagger)
* [RxJava 2](https://github.com/ReactiveX/RxJava)
* [RxAndroid 2](https://github.com/ReactiveX/RxAndroid)
* [Retrofit 2](https://square.github.io/retrofit/)
* [Glide 4](https://github.com/bumptech/glide)
* [RendererRecyclerViewAdapter 2](https://github.com/vivchar/RendererRecyclerViewAdapter)

## Architecture

Mainly the app based on [MVP](https://en.wikipedia.org/wiki/Model–view–presenter) pattern but a view state is managed by [MVI](http://hannesdorfmann.com/android/model-view-intent) approach.
There is no any architecture framework (such as [Moxy](https://github.com/Arello-Mobile/Moxy) or [Mosby](https://github.com/sockeqwe/mosby)) used in the app in order to keep project dependencies minimal.
Since the app has only one screen thus there is no navigation pattern used (e.g. [Cicerone](https://github.com/terrakok/Cicerone) would be preferred)

The library [RendererRecyclerViewAdapter 2](https://github.com/vivchar/RendererRecyclerViewAdapter) used exclusively for experimental purposes.

The app consists of one gradle module only, but it could be divided into separated items:
* app module for final application stuff (Application, resources, dependency graph, high-level build scripts)
* feature modules (such as CurrencyConverter, Currency charts and so on)
* core API module to provide model data from a backend or a repository 
* core utils module to provide low-level framework and common useful code stuff

## Tests

The project includes unit tests only.

## Authors

[Artur Kharchenko](https://www.linkedin.com/in/kharchenko-artur-379731aa) - Android developer in [T-Systems Rus](https://www.t-systems.com/)