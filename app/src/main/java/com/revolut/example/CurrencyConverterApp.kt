package com.revolut.example

import android.app.Application
import com.revolut.example.di.injector.AppInjector
import com.revolut.example.di.injector.Injector
import com.revolut.example.di.injector.InjectorProvider

class CurrencyConverterApp : Application(), InjectorProvider {

    override lateinit var injector: Injector

    override fun onCreate() {
        super.onCreate()
        injector = AppInjector(this)
    }
}