package com.revolut.example.core

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class DefaultSchedulersProvider
@Inject
constructor() : SchedulersProvider {
    override fun newThread() = Schedulers.newThread()

    override fun computation() = Schedulers.computation()

    override fun io() = Schedulers.io()

    override fun mainThread(): Scheduler = AndroidSchedulers.mainThread()
}