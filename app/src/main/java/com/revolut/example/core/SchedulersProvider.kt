package com.revolut.example.core

import io.reactivex.Scheduler

interface SchedulersProvider {

    fun computation(): Scheduler

    fun io(): Scheduler

    fun newThread(): Scheduler

    fun mainThread(): Scheduler
}