package com.revolut.example.di.component

import com.revolut.example.CurrencyConverterApp
import com.revolut.example.di.module.AppModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {

    fun inject(application: CurrencyConverterApp)

    fun plusCurrencyFeatureComponent(): CurrencyFeatureComponent
}