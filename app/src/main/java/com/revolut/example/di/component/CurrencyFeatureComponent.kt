package com.revolut.example.di.component

import com.revolut.example.di.module.ApiModule
import com.revolut.example.di.module.CurrencyFeatureModule
import com.revolut.example.di.scope.PerFeature
import dagger.Subcomponent

@Subcomponent(modules = [CurrencyFeatureModule::class, ApiModule::class])
@PerFeature
interface CurrencyFeatureComponent {

    fun plusCurrencyScreenComponent(): CurrencyScreenComponent

}
