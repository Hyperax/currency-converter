package com.revolut.example.di.component

import com.revolut.example.di.module.CurrencyScreenModule
import com.revolut.example.di.scope.PerScreen
import com.revolut.example.view.CurrenciesListFragment
import dagger.Subcomponent

@Subcomponent(modules = [CurrencyScreenModule::class])
@PerScreen
interface CurrencyScreenComponent {

    fun inject(fragment: CurrenciesListFragment)

}
