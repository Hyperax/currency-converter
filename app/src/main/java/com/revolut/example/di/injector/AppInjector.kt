package com.revolut.example.di.injector

import android.content.Context
import com.revolut.example.di.component.AppComponent
import com.revolut.example.di.component.CurrencyFeatureComponent
import com.revolut.example.di.component.CurrencyScreenComponent
import com.revolut.example.di.component.DaggerAppComponent
import com.revolut.example.di.module.AppModule

class AppInjector(context: Context) : Injector {

    private val appComponent: AppComponent = DaggerAppComponent.builder()
        .appModule(AppModule(context))
        .build()

    private var currencyFeatureComponent: CurrencyFeatureComponent? = null

    private var currencyScreenComponent: CurrencyScreenComponent? = null

    override fun getAppComponent() = appComponent

    override fun getCurrencyFeature(): CurrencyFeatureComponent {
        return currencyFeatureComponent ?: let {
            getAppComponent().plusCurrencyFeatureComponent()
                .also { newComponent -> currencyFeatureComponent = newComponent }
        }
    }

    override fun releaseCurrencyFeature() {
        currencyFeatureComponent = null
    }

    override fun getCurrencyScreen(): CurrencyScreenComponent {
        return currencyScreenComponent ?: let {
            getCurrencyFeature().plusCurrencyScreenComponent()
                .also { newComponent -> currencyScreenComponent = newComponent }
        }
    }

    override fun releaseCurrencyScreen() {
        currencyScreenComponent = null
    }

}