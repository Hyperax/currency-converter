package com.revolut.example.di.injector

import com.revolut.example.di.component.AppComponent
import com.revolut.example.di.component.CurrencyFeatureComponent
import com.revolut.example.di.component.CurrencyScreenComponent

interface Injector {

    fun getAppComponent(): AppComponent

    fun getCurrencyFeature(): CurrencyFeatureComponent
    fun releaseCurrencyFeature()

    fun getCurrencyScreen(): CurrencyScreenComponent
    fun releaseCurrencyScreen()

}