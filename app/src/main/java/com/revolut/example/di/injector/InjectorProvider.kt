package com.revolut.example.di.injector

interface InjectorProvider {
    val injector: Injector
}