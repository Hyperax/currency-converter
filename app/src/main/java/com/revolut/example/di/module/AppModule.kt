package com.revolut.example.di.module

import android.content.Context
import com.revolut.example.core.DefaultSchedulersProvider
import com.revolut.example.core.SchedulersProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val context: Context) {

    @Provides
    @Singleton
    fun provideContext(): Context = context

    @Provides
    @Singleton
    fun provideSchedulers(schedulersProvider: DefaultSchedulersProvider): SchedulersProvider =
        schedulersProvider

}
