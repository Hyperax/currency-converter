package com.revolut.example.di.module

import com.revolut.example.di.scope.PerFeature
import com.revolut.example.model.interactor.Converter
import com.revolut.example.model.interactor.CurrencyInteractor
import com.revolut.example.model.interactor.CurrencyInteractorImpl
import com.revolut.example.model.interactor.SimpleConverter
import com.revolut.example.model.repository.CurrencyRepository
import com.revolut.example.model.repository.CurrencyRepositoryImpl
import com.revolut.example.model.repository.MockCurrencyRepository
import com.revolut.example.presenter.CurrencyImageUrlProvider
import com.revolut.example.presenter.CurrencyImageUrlProviderImpl
import com.revolut.example.presenter.CurrencyMapper
import com.revolut.example.presenter.CurrencyMapperImpl
import dagger.Binds
import dagger.Module
import javax.inject.Named

@Suppress("unused")
@Module
abstract class CurrencyFeatureModule {

    @Binds
    @Named("MOCK")
    @PerFeature
    abstract fun provideMockCurrencyRepository(repository: MockCurrencyRepository): CurrencyRepository

    @Binds
    @Named("REMOTE")
    @PerFeature
    abstract fun provideCurrencyRepository(repository: CurrencyRepositoryImpl): CurrencyRepository

    @Binds
    @PerFeature
    abstract fun provideCurrencyInteractor(interactor: CurrencyInteractorImpl): CurrencyInteractor

    @Binds
    @PerFeature
    abstract fun provideConverter(converter: SimpleConverter): Converter

    @Binds
    @PerFeature
    abstract fun provideImageUrlProvider(provider: CurrencyImageUrlProviderImpl): CurrencyImageUrlProvider

    @Binds
    @PerFeature
    abstract fun providerMapper(mapper: CurrencyMapperImpl): CurrencyMapper
}
