package com.revolut.example.di.module

import com.revolut.example.di.scope.PerScreen
import com.revolut.example.presenter.CurrenciesPresenter
import com.revolut.example.presenter.CurrencyPresenterImpl
import dagger.Binds
import dagger.Module

@Suppress("unused")
@Module
abstract class CurrencyScreenModule {

    @Binds
    @PerScreen
    abstract fun provideCurrencyPresenter(presenter: CurrencyPresenterImpl): CurrenciesPresenter
}
