package com.revolut.example.di.scope

import javax.inject.Scope

@Scope @Retention(AnnotationRetention.RUNTIME)
annotation class PerScreen
