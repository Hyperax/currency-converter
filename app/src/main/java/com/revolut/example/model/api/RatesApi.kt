package com.revolut.example.model.api

import com.revolut.example.model.entity.CurrencyRates
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface RatesApi {

    @GET("latest")
    fun getRates(
        @Query("base") baseCurrencyCode: String
    ): Single<CurrencyRates>

}