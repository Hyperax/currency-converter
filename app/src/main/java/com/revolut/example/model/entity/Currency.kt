package com.revolut.example.model.entity

data class Currency(
    val currencyAlpha3Code: String,
    val countryAlpha2Code: String
)