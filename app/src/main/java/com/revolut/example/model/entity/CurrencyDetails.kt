package com.revolut.example.model.entity

data class CurrencyDetails(
    val currencies: List<Currency>
)