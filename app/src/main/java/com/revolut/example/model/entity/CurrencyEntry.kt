package com.revolut.example.model.entity

import java.math.BigDecimal

data class CurrencyEntry(
    val currencyCode: String = "EUR",
    val amount: BigDecimal = BigDecimal.ONE
)