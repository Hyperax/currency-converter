package com.revolut.example.model.entity

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

data class CurrencyRates(
    @SerializedName("base")
    val baseCurrencyCode: String,
    val rates: Map<String, BigDecimal>
)