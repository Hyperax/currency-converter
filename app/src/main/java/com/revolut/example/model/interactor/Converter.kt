package com.revolut.example.model.interactor

import java.math.BigDecimal

interface Converter {

    fun convert(currencyCode: String, amount: BigDecimal, rate: BigDecimal): BigDecimal
}