package com.revolut.example.model.interactor

import com.revolut.example.model.entity.CurrencyDetails
import com.revolut.example.model.entity.CurrencyRates
import com.revolut.example.model.entity.CurrencyEntry
import io.reactivex.Flowable

interface CurrencyInteractor {

    fun getCurrencyRates(entry: CurrencyEntry): Flowable<CurrencyRates>

    fun getCurrencyDetails(): Flowable<CurrencyDetails>

}