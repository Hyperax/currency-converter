package com.revolut.example.model.interactor

import com.revolut.example.core.SchedulersProvider
import com.revolut.example.model.repository.CurrencyRepository
import com.revolut.example.model.entity.CurrencyDetails
import com.revolut.example.model.entity.CurrencyEntry
import com.revolut.example.model.entity.CurrencyRates
import io.reactivex.Flowable
import io.reactivex.Scheduler
import io.reactivex.functions.Function
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Named

class CurrencyInteractorImpl
@Inject
constructor(
    private val schedulers: SchedulersProvider,
    @Named("REMOTE")
    private val currencyRepository: CurrencyRepository,
    private val converter: Converter) :
    CurrencyInteractor {

    companion object {
        private const val INITIAL_INTERVAL_MILLS = 0L
        private const val POLL_INTERVAL_MILLS = 1000L
        private const val ATTEMPTS_COUNT = 3
        private const val RETRY_DELAY_MILLS = 1000L
        private const val BACKOFF_PERIOD_MILLS = 1000L
    }

    override fun getCurrencyRates(entry: CurrencyEntry): Flowable<CurrencyRates> {
        return Flowable.interval(
            INITIAL_INTERVAL_MILLS,
            POLL_INTERVAL_MILLS,
            TimeUnit.MILLISECONDS,
            schedulers.computation()
        )
            .onBackpressureDrop()
            .flatMapSingle({
                currencyRepository.getCurrencyRates(entry.currencyCode)
                    .map { response -> normalizeRates(response, entry) }
                    .retryWhen(
                        RetryWithDelay(
                            ATTEMPTS_COUNT, RETRY_DELAY_MILLS, BACKOFF_PERIOD_MILLS,
                            scheduler = schedulers.computation()
                        )
                    )
            }, false, 1)
    }

    private fun normalizeRates(response: CurrencyRates, entry: CurrencyEntry): CurrencyRates {
        val normalizedRates = response.rates.mapValues { rateEntry ->
            converter.convert(rateEntry.key, entry.amount, rateEntry.value)
        }
        return response.copy(rates = normalizedRates)
    }

    override fun getCurrencyDetails(): Flowable<CurrencyDetails> {
        return currencyRepository.getCurrenciesDetails()
    }
}

class RetryWithDelay(
    private val maxAttempts: Int,
    private var retryDelay: Long,
    private val backoffPeriod: Long = 0,
    private val unit: TimeUnit = TimeUnit.MILLISECONDS,
    private val scheduler: Scheduler = Schedulers.computation(),
    private val unignorableExceptions: Array<Throwable> = emptyArray())
    : Function<Flowable<out Throwable>, Flowable<*>> {

    private var attempts = 0

    override fun apply(operation: Flowable<out Throwable>): Flowable<*> {
        return operation.flatMap { exception ->
            if (++attempts > maxAttempts || exception in unignorableExceptions) {
                Flowable.error(exception)
            } else {
                retryDelay += backoffPeriod
                Flowable.timer(retryDelay, unit, scheduler)
            }
        }
    }

}