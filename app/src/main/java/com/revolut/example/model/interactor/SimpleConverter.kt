package com.revolut.example.model.interactor

import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*
import javax.inject.Inject

class SimpleConverter
@Inject constructor() : Converter {
    override fun convert(currencyCode: String, amount: BigDecimal, rate: BigDecimal): BigDecimal {
        val scale = Currency.getInstance(currencyCode).defaultFractionDigits

        return (amount * rate).setScale(scale, RoundingMode.HALF_DOWN)
    }
}