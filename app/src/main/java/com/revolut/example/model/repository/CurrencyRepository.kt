package com.revolut.example.model.repository

import com.revolut.example.model.entity.CurrencyDetails
import com.revolut.example.model.entity.CurrencyRates
import io.reactivex.Flowable
import io.reactivex.Single

interface CurrencyRepository {

    fun getCurrencyRates(currencyCode: String): Single<CurrencyRates>

    fun getCurrenciesDetails(): Flowable<CurrencyDetails>

}