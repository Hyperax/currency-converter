package com.revolut.example.model.repository

import com.revolut.example.model.api.RatesApi
import com.revolut.example.model.entity.CurrencyDetails
import com.revolut.example.model.entity.CurrencyRates
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

class CurrencyRepositoryImpl
@Inject
constructor(private val ratesApi: RatesApi) :
    CurrencyRepository {

    override fun getCurrencyRates(currencyCode: String): Single<CurrencyRates> {
        return ratesApi.getRates(currencyCode)
    }

    override fun getCurrenciesDetails(): Flowable<CurrencyDetails> {
        return Flowable.fromCallable { CurrencyDetails(CURRENCY_WITH_COUNTRY_CODES) }
    }

}