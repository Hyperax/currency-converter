package com.revolut.example.model.repository

import com.revolut.example.core.SchedulersProvider
import com.revolut.example.model.entity.CurrencyDetails
import com.revolut.example.model.entity.CurrencyRates
import io.reactivex.Flowable
import io.reactivex.Single
import java.math.BigDecimal
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import kotlin.random.Random

class MockCurrencyRepository
@Inject
constructor(private val schedulersProvider: SchedulersProvider) : CurrencyRepository {

    override fun getCurrencyRates(currencyCode: String): Single<CurrencyRates> {
        return Single.fromCallable {
            CurrencyRates(
                currencyCode,
                RATES.minus(currencyCode).mapValues { entry ->
                    entry.value * BigDecimal("0.${Random.nextInt(85, 99)}")
                }
            )
        }.subscribeOn(schedulersProvider.io())
            .delay(Random.nextLong(0, 2), TimeUnit.SECONDS, schedulersProvider.computation())
    }

    override fun getCurrenciesDetails(): Flowable<CurrencyDetails> {
        return Flowable.fromCallable { CurrencyDetails(CURRENCY_WITH_COUNTRY_CODES) }
            .subscribeOn(schedulersProvider.io())
            .delay(Random.nextLong(0, 2), TimeUnit.SECONDS, schedulersProvider.computation())
    }

}