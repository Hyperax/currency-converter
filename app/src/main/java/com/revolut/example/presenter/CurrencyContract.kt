package com.revolut.example.presenter

import com.github.vivchar.rendererrecyclerviewadapter.ViewModel
import io.reactivex.Flowable
import java.math.BigDecimal

interface CurrenciesPresenter : Presenter<CurrenciesView> {

    fun viewState(): Flowable<CurrencyState>

    fun onEntryChanged(newEntry: RawCurrencyEntry)

    fun onActionRetry()

}

interface CurrenciesView : View

sealed class CurrencyState {
    object Initial : CurrencyState()
    object LoadingProgress : CurrencyState()
    data class LoadingDone(val currencies: List<CurrencyVM>) : CurrencyState()
    data class LoadingError(val exception: Throwable) : CurrencyState()
}

data class CurrencyVM(
    val id: String,
    val name: String,
    val country: String,
    val imageUrl: String?,
    val amount: BigDecimal,
    val isActive: Boolean = false) : ViewModel

data class RawCurrencyEntry(
    val currencyCode: String,
    val amount: String)