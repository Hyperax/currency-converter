package com.revolut.example.presenter

import com.revolut.example.model.entity.Currency

interface CurrencyImageUrlProvider {

    fun getImageUrl(currency: Currency): String?

}