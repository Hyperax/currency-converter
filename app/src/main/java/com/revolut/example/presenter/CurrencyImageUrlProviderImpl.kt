package com.revolut.example.presenter

import com.revolut.example.model.entity.Currency
import javax.inject.Inject

class CurrencyImageUrlProviderImpl
@Inject constructor() : CurrencyImageUrlProvider {

    private val urlCache = mutableMapOf<String, String>()

    override fun getImageUrl(currency: Currency): String? {
        return urlCache[currency.countryAlpha2Code] ?: let {
            val url = "https://countryflags.io/${currency.countryAlpha2Code}/shiny/64.png"
            urlCache[currency.countryAlpha2Code] = url
            url
        }
    }
}