package com.revolut.example.presenter

import com.revolut.example.model.entity.Currency
import com.revolut.example.model.entity.CurrencyEntry
import com.revolut.example.model.entity.CurrencyRates

interface CurrencyMapper {

    fun map(
        baseCurrency: CurrencyEntry,
        currencies: Map<String, Currency>,
        currencyRates: CurrencyRates): List<CurrencyVM>

}