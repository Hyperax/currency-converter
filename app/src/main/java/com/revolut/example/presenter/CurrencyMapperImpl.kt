package com.revolut.example.presenter

import com.revolut.example.model.entity.Currency
import com.revolut.example.model.entity.CurrencyEntry
import com.revolut.example.model.entity.CurrencyRates
import java.math.BigDecimal
import java.util.*
import javax.inject.Inject

class CurrencyMapperImpl
@Inject
constructor(
    private val imageProvider: CurrencyImageUrlProvider) :
    CurrencyMapper {

    override fun map(
        baseCurrency: CurrencyEntry,
        currencies: Map<String, Currency>,
        currencyRates: CurrencyRates): List<CurrencyVM> {

        return mapOf(baseCurrency.currencyCode to baseCurrency.amount)
            .plus(currencyRates.rates).map { currencyWithRate ->
                val code = currencyWithRate.key
                val currency = currencies[code] ?: Currency(code, code)
                CurrencyVM(
                    code,
                    java.util.Currency.getInstance(currency.currencyAlpha3Code).displayName,
                    Locale("", currency.countryAlpha2Code).displayCountry,
                    imageProvider.getImageUrl(currency),
                    currencyWithRate.value,
                    baseCurrency.currencyCode == code
                )
            }
    }
}