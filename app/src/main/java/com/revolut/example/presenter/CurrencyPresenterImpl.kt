package com.revolut.example.presenter

import com.revolut.example.core.SchedulersProvider
import com.revolut.example.model.interactor.CurrencyInteractor
import com.revolut.example.model.entity.Currency
import com.revolut.example.model.entity.CurrencyEntry
import com.revolut.example.utils.toBigDecimalOrZero
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.BiFunction
import io.reactivex.subjects.BehaviorSubject
import java.math.BigDecimal
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class CurrencyPresenterImpl
@Inject
constructor(
    private val schedulers: SchedulersProvider,
    private val currencyInteractor: CurrencyInteractor,
    private val currencyViewMapper: CurrencyMapper) : CurrenciesPresenter {

    private val viewStateProvider =
        BehaviorSubject.createDefault<CurrencyState>(CurrencyState.Initial)

    private val compositeDisposable = CompositeDisposable()

    private val entrySubject = BehaviorSubject.createDefault<CurrencyEntry>(CurrencyEntry())

    override fun viewState(): Flowable<CurrencyState> {
        return viewStateProvider.toFlowable(BackpressureStrategy.LATEST)
            .doOnSubscribe {
                if (compositeDisposable.size() == 0) {
                    initialObserveRates()
                }
            }
            .doOnCancel { compositeDisposable.clear() }
    }

    private fun initialObserveRates() {
        state = CurrencyState.LoadingProgress

        val currencyEntry = entrySubject.toFlowable(BackpressureStrategy.LATEST)
            .debounce(300, TimeUnit.MILLISECONDS, schedulers.computation())
            .distinctUntilChanged()

        val detailsFlow = currencyInteractor.getCurrencyDetails()
            .observeOn(schedulers.computation())
            .map { details -> details.currencies.associateBy { it.currencyAlpha3Code } }

        val combinedChanges: Flowable<Pair<CurrencyEntry, Map<String, Currency>>> =
            Flowable.combineLatest(currencyEntry, detailsFlow,
                BiFunction { entry, details -> entry to details })

        combinedChanges.switchMap { (entry, details) ->
            currencyInteractor.getCurrencyRates(entry)
                .map { currencyRates -> Triple(entry, details, currencyRates) }
        }
            .map { (entry, details, currencyRates) ->
                currencyViewMapper.map(entry, details, currencyRates)
            }
            .map<CurrencyState> { CurrencyState.LoadingDone(it) }
            .onErrorReturn { CurrencyState.LoadingError(it) }
            .subscribe { state = it }
            .also { compositeDisposable.add(it) }
    }

    override fun onEntryChanged(newEntry: RawCurrencyEntry) {
        val amountEntry = if (newEntry.amount.isEmpty()) {
            BigDecimal.ONE
        } else {
            newEntry.amount.toBigDecimalOrZero()
        }
        entrySubject.onNext(CurrencyEntry(newEntry.currencyCode, amountEntry))
    }

    override fun onActionRetry() {
        compositeDisposable.clear()
        initialObserveRates()
    }

    override fun onFinish() {
        compositeDisposable.clear()
    }

    private var state: CurrencyState
        get() = viewStateProvider.value!!
        set(value) = viewStateProvider.onNext(value)

}