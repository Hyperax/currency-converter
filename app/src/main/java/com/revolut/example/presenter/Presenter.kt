package com.revolut.example.presenter

interface Presenter<V : View> {
    fun onFinish()
}