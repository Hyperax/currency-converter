package com.revolut.example.utils

import java.math.BigDecimal

fun Int.containsBinary(anotherInt: Int) = (this and anotherInt) == anotherInt

fun String.toBigDecimalOrZero(): BigDecimal = this.toBigDecimalOrNull() ?: BigDecimal.ZERO