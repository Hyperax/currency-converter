package com.revolut.example.utils

import android.text.InputFilter
import android.text.Spanned
import java.util.regex.Pattern


class RegexInputFilter(stringPattern: String = "", pattern: Pattern? = null) : InputFilter {

    private val pattern = pattern ?: stringPattern.toPattern()

    override fun filter(
        source: CharSequence, start: Int, end: Int,
        dest: Spanned, dstart: Int, dend: Int): CharSequence? {

        val toCheck = dest.subSequence(0, dstart).toString() +
                source.subSequence(start, end) +
                dest.subSequence(dend, dest.length).toString()

        return if (pattern.matcher(toCheck).matches()) null else ""
    }
}