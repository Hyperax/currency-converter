package com.revolut.example.view

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.vivchar.rendererrecyclerviewadapter.DefaultDiffCallback
import com.github.vivchar.rendererrecyclerviewadapter.RendererRecyclerViewAdapter
import com.revolut.example.R
import com.revolut.example.core.SchedulersProvider
import com.revolut.example.presenter.CurrenciesPresenter
import com.revolut.example.presenter.CurrenciesView
import com.revolut.example.presenter.CurrencyState
import com.revolut.example.presenter.CurrencyVM
import com.revolut.example.utils.hideSoftKeyboard
import com.revolut.example.view.Payloads.ACTIVATION
import com.revolut.example.view.Payloads.AMOUNT
import com.revolut.example.view.Payloads.DATA
import com.revolut.example.view.Payloads.EMPTY
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.fragment_currencies_list.*
import javax.inject.Inject

class CurrenciesListFragment : InjectableFragment(), CurrenciesView {

    @Inject
    internal lateinit var schedulers: SchedulersProvider

    @Inject
    internal lateinit var presenter: CurrenciesPresenter

    private lateinit var recyclerAdapter: RendererRecyclerViewAdapter

    private var stateChangesDisposable: Disposable? = null

    private var activeCurrencyCode: String? = null

    override fun onAttach(context: Context) {
        injector.getCurrencyScreen().inject(this)
        super.onAttach(context)
        recyclerAdapter = RendererRecyclerViewAdapter()

        recyclerAdapter.registerRenderer(CurrencyViewBinder {
            presenter.onEntryChanged(it)
        })
        recyclerAdapter.enableDiffUtil()
        recyclerAdapter.setDiffCallback(PayloadDiffCallback())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_currencies_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(currencies_recycler_view) {
            adapter = recyclerAdapter
            layoutManager = LinearLayoutManager(requireContext())
            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                        hideSoftKeyboard()
                    }
                }
            })
        }

        retry_button.setOnClickListener { presenter.onActionRetry() }
    }

    override fun onStart() {
        super.onStart()
        stateChangesDisposable = presenter.viewState()
            .subscribeOn(schedulers.computation())
            .observeOn(schedulers.mainThread())
            .subscribe(this::handleState)
    }

    override fun onStop() {
        super.onStop()
        stateChangesDisposable?.dispose()
        if (activity?.isFinishing == true) {
            injector.releaseCurrencyScreen()
            presenter.onFinish()
        }
    }

    private fun handleState(state: CurrencyState) {
        when (state) {
            is CurrencyState.LoadingProgress -> showProgress()
            is CurrencyState.LoadingDone -> showCurrencies(state)
            is CurrencyState.LoadingError -> showError(state)
        }
    }

    private fun showError(state: CurrencyState.LoadingError) {
        currencies_recycler_view.hideSoftKeyboard()
        content_group.visibility = View.GONE
        progress_group.visibility = View.GONE
        info_textview.text = getString(R.string.error_with_description, "\n${state.exception}")
        error_group.visibility = View.VISIBLE
    }

    private fun showProgress() {
        progress_group.visibility = View.VISIBLE
        currencies_recycler_view.hideSoftKeyboard()
        content_group.visibility = View.GONE
        error_group.visibility = View.GONE
    }

    private fun showCurrencies(state: CurrencyState.LoadingDone) {
        recyclerAdapter.setItems(state.currencies)
        progress_group.visibility = View.GONE
        error_group.visibility = View.GONE
        content_group.visibility = View.VISIBLE

        state.currencies.firstOrNull { it.isActive }
            ?.takeIf { it.id != activeCurrencyCode }
            ?.run {
                activeCurrencyCode = this.id
                currencies_recycler_view.scrollToPosition(0)
            }
    }
}

class PayloadDiffCallback : DefaultDiffCallback<CurrencyVM>() {

    override fun areItemsTheSame(oldItem: CurrencyVM, newItem: CurrencyVM): Boolean {
        return oldItem.id == newItem.id
    }

    override fun getChangePayload(oldItem: CurrencyVM, newItem: CurrencyVM): Any? {
        var payloads = EMPTY
        if (!newItem.isActive && oldItem.amount != newItem.amount) {
            payloads = payloads or AMOUNT
        }
        if (oldItem.country != newItem.country
            || oldItem.imageUrl != newItem.imageUrl
            || oldItem.name != newItem.name) {
            payloads = payloads or DATA
        }

        if (oldItem.isActive != newItem.isActive) {
            payloads = payloads or ACTIVATION
        }
        return payloads
    }
}

object Payloads {
    const val EMPTY = 0
    const val AMOUNT = 1
    const val DATA = 2
    const val ACTIVATION = 4
}