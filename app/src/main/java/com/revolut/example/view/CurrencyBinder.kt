package com.revolut.example.view

import android.text.Editable
import android.text.TextWatcher
import android.widget.ImageView
import android.widget.TextView
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewBinder
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewFinder
import com.revolut.example.R
import com.revolut.example.di.module.GlideApp
import com.revolut.example.presenter.CurrencyVM
import com.revolut.example.presenter.RawCurrencyEntry
import com.revolut.example.utils.RegexInputFilter
import com.revolut.example.utils.containsBinary
import com.revolut.example.utils.showSoftKeyboard

class CurrencyViewBinder(
    onEntryChangeListener: (RawCurrencyEntry) -> Unit
) : ViewBinder<CurrencyVM>(
    R.layout.item_currency,
    CurrencyVM::class.java,
    CurrencyBinder(onEntryChangeListener)
)

class CurrencyBinder(
    private val onEntryChangeListener: (RawCurrencyEntry) -> Unit
) : ViewBinder.Binder<CurrencyVM> {

    override fun bindView(currencyVM: CurrencyVM, viewFinder: ViewFinder, payloads: MutableList<Any>) {
        if (payloads.isEmpty()) {
            updateDetails(currencyVM, viewFinder)
            if (currencyVM.isActive) {
                updateActiveInput(currencyVM, viewFinder)
            } else {
                updateInactiveInput(currencyVM, viewFinder)
            }
            updateAmount(currencyVM, viewFinder)
        } else {
            updatePayloads(currencyVM, viewFinder, payloads.first() as Int)
        }
    }

    private fun updatePayloads(currencyVM: CurrencyVM, viewFinder: ViewFinder, actualPayloads: Int) {
        if (actualPayloads.containsBinary(Payloads.DATA)) {
            updateDetails(currencyVM, viewFinder)
        }

        if (actualPayloads.containsBinary(Payloads.ACTIVATION)
            || actualPayloads.containsBinary(Payloads.AMOUNT)) {
            if (currencyVM.isActive) {
                updateActiveInput(currencyVM, viewFinder, true)
            } else {
                updateInactiveInput(currencyVM, viewFinder)
                updateAmount(currencyVM, viewFinder)
            }
        }
    }

    private fun updateActiveInput(currencyVM: CurrencyVM, viewFinder: ViewFinder, requestInput: Boolean = false) {
        with(viewFinder.find<TextView>(R.id.amount)) {
            if (requestInput) {
                requestFocus()
                showSoftKeyboard()
            }
            addTextWatcher(currencyVM)
            onFocusChangeListener = null
        }
    }

    private fun TextView.addTextWatcher(currencyVM: CurrencyVM) {
        (tag as? TextWatcher) ?: run {
            val textWatcher = SimpleTextWatcher { text ->
                onEntryChangeListener(RawCurrencyEntry(currencyVM.id, text.toString()))
            }
            addTextChangedListener(textWatcher)

            tag = textWatcher
        }
    }

    private fun updateInactiveInput(currencyVM: CurrencyVM, viewFinder: ViewFinder) {
        with(viewFinder.find<TextView>(R.id.amount)) {
            clearFocus()
            removeTextWatcher()

            setOnFocusChangeListener { _, hasFocus ->
                if (hasFocus) {
                    onEntryChangeListener(RawCurrencyEntry(currencyVM.id, text.toString()))
                }
            }
        }
    }

    private fun TextView.removeTextWatcher() {
        (tag as? TextWatcher)?.run {
            removeTextChangedListener(this)
            tag = null
        }
    }

    private fun updateDetails(currencyVM: CurrencyVM, viewFinder: ViewFinder) {
        viewFinder.find<TextView>(R.id.currency_name).text = currencyVM.name
        viewFinder.find<TextView>(R.id.country_name).text = currencyVM.country

        with(viewFinder.find<ImageView>(R.id.country_flag)) {
            GlideApp
                .with(this)
                .load(currencyVM.imageUrl)
                .placeholder(R.drawable.ic_flag_placeholder)
                .error(R.drawable.ic_flag_placeholder)
                .into(this)
        }
    }

    private fun updateAmount(currencyVM: CurrencyVM, viewFinder: ViewFinder) {
        with(viewFinder.find<TextView>(R.id.amount)) {
            filters = arrayOf(getInputFilter(currencyVM.amount.scale()))
            text = currencyVM.amount.toPlainString()
        }
    }

    private fun getInputFilter(scale: Int) = if (scale > 0) {
        RegexInputFilter("[0-9]*([,.][0-9]{0,$scale})?")
    } else {
        RegexInputFilter("[0-9]*")
    }
}

class SimpleTextWatcher(val action: (CharSequence) -> Unit) : TextWatcher {
    override fun afterTextChanged(s: Editable) = action(s.toString())

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = Unit
}