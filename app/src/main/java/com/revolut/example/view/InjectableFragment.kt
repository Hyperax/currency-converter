package com.revolut.example.view

import android.support.v4.app.Fragment
import com.revolut.example.di.injector.Injector
import com.revolut.example.di.injector.InjectorProvider

abstract class InjectableFragment : Fragment() {

    protected val injector: Injector
        get() = (requireContext().applicationContext as InjectorProvider).injector

}