package com.revolut.example.view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.revolut.example.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.content, CurrenciesListFragment())
                .commit()
        }
    }
}
