package com.revolut.example.api

import com.revolut.example.model.api.RatesApi
import com.revolut.example.model.entity.CurrencyRates
import io.reactivex.Single

class MockRatesApi : RatesApi {

    var mockedResult: Single<CurrencyRates> = Single.error<CurrencyRates>(
        IllegalStateException("No mock result provided")
    )

    override fun getRates(baseCurrencyCode: String) = mockedResult
}