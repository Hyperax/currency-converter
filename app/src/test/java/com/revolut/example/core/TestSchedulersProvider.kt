package com.revolut.example.core

import io.reactivex.Scheduler
import io.reactivex.schedulers.TestScheduler
import javax.inject.Inject

class TestSchedulersProvider
@Inject
constructor(private val defaultScheduler: Scheduler = TestScheduler()) : SchedulersProvider {
    override fun newThread() = defaultScheduler

    override fun computation() = defaultScheduler

    override fun io() = defaultScheduler

    override fun mainThread(): Scheduler = defaultScheduler
}