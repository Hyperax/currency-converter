@file:Suppress("MemberVisibilityCanBePrivate")

package com.revolut.example.model.interactor

import com.revolut.example.core.TestSchedulersProvider
import com.revolut.example.model.entity.CurrencyDetails
import com.revolut.example.model.entity.CurrencyEntry
import com.revolut.example.model.entity.CurrencyRates
import io.reactivex.Single
import io.reactivex.schedulers.TestScheduler
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import java.lang.RuntimeException
import java.math.BigDecimal
import java.util.concurrent.TimeUnit

class CurrencyInteractorImplTest {

    val testCurrencyCode: String = "EUR"
    val testScheduler = TestScheduler()
    val testSchedulersProvider = TestSchedulersProvider(testScheduler)
    val testRepository = TestCurrencyRepository(testScheduler)

    val dummyConverter = object : Converter {
        override fun convert(currencyCode: String, amount: BigDecimal, rate: BigDecimal) =
            amount * rate
    }

    val testCurrencyEntry = CurrencyEntry(testCurrencyCode, BigDecimal.TEN)

    private val testRates = CurrencyRates(
        testCurrencyCode,
        mapOf(
            "USD" to BigDecimal("1.09"),
            "RUB" to BigDecimal("75.12")
        )
    )

    private val normalizedTestRates = testRates.copy(
        rates = testRates.rates.mapValues { entry ->
            dummyConverter.convert(entry.key, testCurrencyEntry.amount, entry.value)
        }
    )

    lateinit var interactor: CurrencyInteractorImpl

    @Before
    fun setUp() {
        interactor = CurrencyInteractorImpl(testSchedulersProvider, testRepository, dummyConverter)
    }

    @Test
    fun `interactor returns valid details`() {
        val testExpectSubscriber = testRepository.getCurrenciesDetails().test()
        val testActualSubscriber = interactor.getCurrencyDetails().test()

        testScheduler.advanceTimeBy(3, TimeUnit.SECONDS)
        testRepository.emitDetails(CurrencyDetails(emptyList()))
        testScheduler.advanceTimeBy(3, TimeUnit.SECONDS)

        testActualSubscriber.assertNoErrors()
        testActualSubscriber.assertValueCount(2)
        testActualSubscriber.assertNotComplete()

        testActualSubscriber.assertValueAt(0, testExpectSubscriber.values()[0])
        testActualSubscriber.assertValueAt(1, testExpectSubscriber.values()[1])
    }

    @Test
    fun `interactor returns valid rates`() {
        testRepository.currencyRates = Single.just(testRates)
        val testActualSubscriber = interactor.getCurrencyRates(testCurrencyEntry).test()

        val timeInterval = 10L
        testScheduler.advanceTimeBy(timeInterval, TimeUnit.SECONDS)

        testActualSubscriber.assertNoErrors()
        val emits = testActualSubscriber.valueCount()
        val minimalRatesCount = (0.9 * timeInterval).toInt()
        assertTrue(
            "Interactor should emit at least $minimalRatesCount rates within $timeInterval seconds. Actual count: $emits",
            emits >= minimalRatesCount
        )
        testActualSubscriber.assertNotComplete()

        testActualSubscriber.assertValueAt(emits - 1) { actualValue ->
            assertEquals(
                "Interactor should return the same base currency as an argument",
                actualValue.baseCurrencyCode, testCurrencyCode
            )
            assertEquals(
                "Interactor should return normalized rates in respect of base currency",
                actualValue, normalizedTestRates
            )
            true
        }
    }

    @Test
    fun `interactor persists at least one error`() {
        testRepository.currencyRates = Single.error(RuntimeException("Test exception"))
        val testActualSubscriber = interactor.getCurrencyRates(testCurrencyEntry).test()

        val timeInterval = 3L
        testScheduler.advanceTimeBy(timeInterval, TimeUnit.SECONDS)

        testActualSubscriber.assertValueCount(0)
        testActualSubscriber.assertNoErrors()
        testActualSubscriber.assertNotComplete()
        testActualSubscriber.assertNotTerminated()
    }

    @Test
    fun `interactor throws an exception in long-term errors`() {
        val testException = RuntimeException("Test exception")
        testRepository.currencyRates = Single.error(testException)
        val testActualSubscriber = interactor.getCurrencyRates(testCurrencyEntry).test()

        val timeInterval = 10L
        testScheduler.advanceTimeBy(timeInterval, TimeUnit.SECONDS)

        testActualSubscriber.assertError(testException)
        testActualSubscriber.assertTerminated()
    }
}