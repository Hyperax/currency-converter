package com.revolut.example.model.interactor

import com.revolut.example.utils.compareEquals
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import java.math.BigDecimal

class SimpleConverterTest {

    private val ONE_HUNDRED = BigDecimal("100")

    private val ARRAY_OF_AMOUNTS = arrayOf(
        BigDecimal.ZERO, BigDecimal.ONE, BigDecimal.TEN,
        ONE_HUNDRED, BigDecimal("100.12"), BigDecimal("123.456")
    )

    private val CURRENCY_TWO_DEC_PLACES = "EUR"
    private val CURRENCY_ZERO_DEC_PLACES = "JPY"
    private val CURRENCY_THREE_DEC_PLACES = "JOD"

    private lateinit var converter: SimpleConverter

    @Before
    fun setUp() {
        converter = SimpleConverter()
    }

    @Test
    fun `converting respects decimal places of currency`() {
        ARRAY_OF_AMOUNTS.forEach { testAmount ->
            assertTrue(
                "Currency with 2 decimal places should have scale lesser than 3",
                converter.convert(CURRENCY_TWO_DEC_PLACES, BigDecimal.ONE, testAmount).scale() < 3
            )
        }

        ARRAY_OF_AMOUNTS.forEach { testAmount ->
            assertEquals(
                "Currency with 0 decimal places should have zero-scale",
                converter.convert(CURRENCY_ZERO_DEC_PLACES, BigDecimal.ONE, testAmount).scale(), 0
            )
        }

        ARRAY_OF_AMOUNTS.forEach { testAmount ->
            assertTrue(
                "Currency with 3 decimal places should have scale lesser than 4",
                converter.convert(CURRENCY_THREE_DEC_PLACES, BigDecimal.ONE, testAmount).scale() < 4
            )
        }
    }

    @Test
    fun `converting calculates correctly`() {
        assertTrue(
            "1 euro equals 0 for rate 0",
            converter.convert(CURRENCY_TWO_DEC_PLACES, BigDecimal.ONE, BigDecimal.ZERO)
                .compareEquals(BigDecimal.ZERO)
        )

        assertTrue(
            "1 euro equals 1 for rate 1",
            converter.convert(CURRENCY_TWO_DEC_PLACES, BigDecimal.ONE, BigDecimal.ONE)
                .compareEquals(BigDecimal.ONE)
        )

        assertTrue(
            "1 euro equals 100 for rate 100",
            converter.convert(CURRENCY_TWO_DEC_PLACES, BigDecimal.ONE, ONE_HUNDRED)
                .compareEquals(ONE_HUNDRED)
        )

        assertTrue(
            "100 euro equals 10_000 for rate 100",
            converter.convert(CURRENCY_TWO_DEC_PLACES, ONE_HUNDRED, ONE_HUNDRED)
                .compareEquals(ONE_HUNDRED * ONE_HUNDRED)
        )
    }

    @Test
    fun `converting respects rounding`() {
        assertTrue(
            "1 euro equals 0.23 for rate 0.234",
            converter.convert(CURRENCY_TWO_DEC_PLACES, BigDecimal.ONE, BigDecimal("0.234"))
                .compareEquals(BigDecimal("0.23"))
        )

        assertTrue(
            "1 euro equals 0.24 for rate 0.236",
            converter.convert(CURRENCY_TWO_DEC_PLACES, BigDecimal.ONE, BigDecimal("0.236"))
                .compareEquals(BigDecimal("0.24"))
        )

        assertTrue(
            "12.34 euro equals 152.34 for rate 12.345",
            converter.convert(CURRENCY_TWO_DEC_PLACES, BigDecimal("12.34"), BigDecimal("12.345"))
                .compareEquals(BigDecimal("152.34"))
        )
    }

}