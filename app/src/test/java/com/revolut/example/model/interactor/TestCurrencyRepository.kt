package com.revolut.example.model.interactor

import com.revolut.example.model.entity.CurrencyDetails
import com.revolut.example.model.entity.CurrencyRates
import com.revolut.example.model.repository.CURRENCY_WITH_COUNTRY_CODES
import com.revolut.example.model.repository.CurrencyRepository
import com.revolut.example.model.repository.RATES
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.subjects.BehaviorSubject
import java.math.BigDecimal
import java.util.concurrent.TimeUnit
import kotlin.random.Random

class TestCurrencyRepository(private val scheduler: Scheduler) : CurrencyRepository {

    private val detailsSubject =
        BehaviorSubject.createDefault(CurrencyDetails(CURRENCY_WITH_COUNTRY_CODES))

    var currencyRates: Single<CurrencyRates>? = null

    override fun getCurrencyRates(currencyCode: String): Single<CurrencyRates> {
        return (currencyRates
            ?: throw IllegalArgumentException("currency rates are not provided for mock data"))
            .subscribeOn(scheduler)
    }

    override fun getCurrenciesDetails(): Flowable<CurrencyDetails> {
        return detailsSubject.toFlowable(BackpressureStrategy.LATEST)
            .subscribeOn(scheduler)
    }

    fun emitDetails(details: CurrencyDetails) = detailsSubject.onNext(details)

}