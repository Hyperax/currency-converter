package com.revolut.example.model.repository

import com.revolut.example.api.MockRatesApi
import com.revolut.example.model.entity.CurrencyDetails
import com.revolut.example.model.entity.CurrencyRates
import io.reactivex.Single
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class CurrencyRepositoryImplTest {

    private val currencyCode = "EUR"

    private lateinit var mockRatesApi: MockRatesApi
    private lateinit var repository: CurrencyRepositoryImpl

    @Before
    fun setUp() {
        mockRatesApi = MockRatesApi()
        repository = CurrencyRepositoryImpl(mockRatesApi)
    }

    @Test
    fun `repository returns actual rates`() {
        val testRates = CurrencyRates(currencyCode, RATES)
        mockRatesApi.mockedResult = Single.just(testRates)

        val testObserver = repository.getCurrencyRates(currencyCode)
            .test()

        testObserver.assertComplete()
        testObserver.assertValueCount(1)
        testObserver.assertValue { actualValue ->
            assertEquals(
                "Base currency code must be the same as requested",
                actualValue.baseCurrencyCode, currencyCode
            )
            assertEquals(
                "Rates count incorrect",
                testRates.rates.size, actualValue.rates.size
            )
            true
        }
    }

    @Test
    fun `repository crashes on error rates result`() {
        val exception = RuntimeException("Test exception")
        mockRatesApi.mockedResult = Single.error(exception)

        val testObserver = repository.getCurrencyRates(currencyCode)
            .test()

        testObserver.assertError(exception)
    }

    @Test
    fun `repository returns const currencies details`() {
        val testSubscriber = repository.getCurrenciesDetails()
            .test()

        testSubscriber.assertComplete()
        testSubscriber.assertValueCount(1)
        testSubscriber.hasSubscription()
        testSubscriber.assertValue(CurrencyDetails(CURRENCY_WITH_COUNTRY_CODES))
    }
}