@file:Suppress("MemberVisibilityCanBePrivate")

package com.revolut.example.presenter

import com.revolut.example.core.TestSchedulersProvider
import com.revolut.example.model.entity.Currency
import com.revolut.example.model.entity.CurrencyDetails
import com.revolut.example.model.entity.CurrencyEntry
import com.revolut.example.model.entity.CurrencyRates
import com.revolut.example.model.interactor.CurrencyInteractor
import com.revolut.example.model.repository.CURRENCY_WITH_COUNTRY_CODES
import com.revolut.example.model.repository.RATES
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.schedulers.TestScheduler
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subscribers.TestSubscriber
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import java.lang.RuntimeException
import java.math.BigDecimal
import java.util.concurrent.TimeUnit

class CurrencyPresenterImplTest {

    val testBaseCurrency = "EUR"
    val testEntry = CurrencyEntry(testBaseCurrency, BigDecimal.TEN)
    val testRawEntry = RawCurrencyEntry(testBaseCurrency, "10")
    val testCurrencyDetails = CurrencyDetails(CURRENCY_WITH_COUNTRY_CODES)
    val testRates = CurrencyRates(testBaseCurrency, RATES)
    val testException = RuntimeException("Test exception")


    val testScheduler = TestScheduler()
    val testSchedulers = TestSchedulersProvider(testScheduler)
    var dummyMapper = DummyMapper()

    lateinit var testInteractor: TestInteractor
    lateinit var presenter: CurrencyPresenterImpl

    @Before
    fun setUp() {
        testInteractor = TestInteractor()
        presenter = CurrencyPresenterImpl(testSchedulers, testInteractor, dummyMapper)
    }

    @Test
    fun `presenter returns progress state on first subscription`() {
        val testSubscriber = presenter.viewState().test()

        testSubscriber.assertNotComplete()
        testSubscriber.assertNoErrors()
        testSubscriber.assertValue(CurrencyState.LoadingProgress)
    }

    @Test
    fun `presenter provides actual rates`() {
        val testSubscriber = attachViewOnPresenter()

        presenter.onEntryChanged(testRawEntry)
        testScheduler.advanceTimeBy(1, TimeUnit.SECONDS)

        testSubscriber.assertNotComplete()
        testSubscriber.assertNoErrors()
        val expectedData = dummyMapper.map(
            testEntry,
            testCurrencyDetails.currencies.associateBy { it.currencyAlpha3Code }, testRates
        )
        assertEquals(
            "Presenter should return data on entry change",
            CurrencyState.LoadingDone(expectedData), testSubscriber.values().last()
        )
    }

    @Test
    fun `presenter returns error view state`() {
        val testSubscriber = attachViewOnPresenter()
        testInteractor.ratesSubject.onError(testException)
        testScheduler.advanceTimeBy(1, TimeUnit.SECONDS)

        testSubscriber.assertNoErrors()
        assertEquals(
            "Presenter should return error view state",
            CurrencyState.LoadingError(testException), testSubscriber.values().last()
        )
    }

    @Test
    fun `presenter correctly restart its lifecycle`() {
        val testSubscriber = attachViewOnPresenter()

        testInteractor.ratesSubject.onError(testException)
        testScheduler.advanceTimeBy(1, TimeUnit.SECONDS)

        testInteractor.ratesSubject = BehaviorSubject.createDefault(testRates)
        presenter.onActionRetry()
        testScheduler.advanceTimeBy(1, TimeUnit.SECONDS)

        assertTrue(
            "Presenter should restart emitting of states after retry action ",
            testSubscriber.values().last() is CurrencyState.LoadingDone
        )
    }

    private fun attachViewOnPresenter(): TestSubscriber<CurrencyState> {
        val testSubscriber = presenter.viewState().test()
        testInteractor.detailsSubject.onNext(testCurrencyDetails)
        testInteractor.ratesSubject.onNext(testRates)
        testScheduler.advanceTimeBy(1, TimeUnit.SECONDS)
        return testSubscriber
    }
}

class TestInteractor : CurrencyInteractor {
    var ratesSubject = BehaviorSubject.create<CurrencyRates>()
    var detailsSubject = BehaviorSubject.create<CurrencyDetails>()

    override fun getCurrencyRates(entry: CurrencyEntry): Flowable<CurrencyRates> =
        ratesSubject.toFlowable(BackpressureStrategy.LATEST)

    override fun getCurrencyDetails(): Flowable<CurrencyDetails> =
        detailsSubject.toFlowable(BackpressureStrategy.LATEST)
}

class DummyMapper : CurrencyMapper {
    override fun map(
        baseCurrency: CurrencyEntry,
        currencies: Map<String, Currency>,
        currencyRates: CurrencyRates): List<CurrencyVM> {
        return mapOf(baseCurrency.currencyCode to baseCurrency.amount)
            .plus(currencyRates.rates).map { currencyWithRate ->
                val code = currencyWithRate.key
                CurrencyVM(
                    code,
                    code,
                    code,
                    "image url for $code",
                    currencyWithRate.value,
                    baseCurrency.currencyCode == code
                )
            }
    }

}