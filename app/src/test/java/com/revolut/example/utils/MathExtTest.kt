package com.revolut.example.utils

import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import java.math.BigDecimal

class MathExtTest {

    @Test
    fun `contains binary is correct`() {
        assertTrue("Any binary should contain zero", 0.containsBinary(0))
        assertTrue("Any binary should contain zero", Int.MAX_VALUE.containsBinary(0))
        assertTrue("Any binary should contain zero", Int.MIN_VALUE.containsBinary(0))

        assertTrue("binary 0001 should contain binary 0001", 1.containsBinary(1))
        assertTrue("binary 0111 should contain binary 0001", 3.containsBinary(1))
        assertTrue("binary 0111 should contain binary 0010", 3.containsBinary(2))

        assertFalse("binary 0001 should not contain binary 0010", 1.containsBinary(2))
        assertFalse("binary 0010 should not contain binary 0001", 2.containsBinary(1))
        assertFalse("binary 0111 should not contain binary 1000", 3.containsBinary(4))
    }

    @Test
    fun `String to BigDecimal converting is correct`() {
        assertTrue(
            "Empty string should be zero",
            "".toBigDecimalOrZero().compareEquals(BigDecimal.ZERO)
        )
        assertTrue(
            "'0' string should be zero",
            "0".toBigDecimalOrZero().compareEquals(BigDecimal.ZERO)
        )
        assertTrue(
            "'0.00' should be zero",
            "0.00".toBigDecimalOrZero().compareEquals(BigDecimal.ZERO)
        )
        assertTrue(
            "Any incorrect input should be zero",
            "-0".toBigDecimalOrZero().compareEquals(BigDecimal.ZERO)
        )
        assertTrue(
            "Any incorrect input should be zero",
            "Any input".toBigDecimalOrZero().compareEquals(BigDecimal.ZERO)
        )

        assertTrue(
            "'1' should be 1",
            "1".toBigDecimalOrZero().compareEquals(BigDecimal.ONE)
        )
        assertTrue(
            "'1.' should be 1",
            "1.".toBigDecimalOrZero().compareEquals(BigDecimal.ONE)
        )
        assertTrue(
            "'1.00' should be 1",
            "1.00".toBigDecimalOrZero().compareEquals(BigDecimal.ONE)
        )

        assertTrue(
            "'-1' should be -1",
            "-1".toBigDecimalOrZero().compareEquals(-BigDecimal.ONE)
        )
        assertTrue(
            "'-1.' should be -1",
            "-1.".toBigDecimalOrZero().compareEquals(-BigDecimal.ONE)
        )
        assertTrue(
            "'-1.00' should be -1",
            "-1.000".toBigDecimalOrZero().compareEquals(-BigDecimal.ONE)
        )

        assertTrue(
            "'10' should be 10",
            "10".toBigDecimalOrZero().compareEquals(BigDecimal.TEN)
        )
        assertTrue(
            "'10.' should be 10",
            "10.".toBigDecimalOrZero().compareEquals(BigDecimal.TEN)
        )
        assertTrue(
            "'10.00' should be 10",
            "10.00".toBigDecimalOrZero().compareEquals(BigDecimal.TEN)
        )

        assertTrue(
            "'-10' should be -10",
            "-10".toBigDecimalOrZero().compareEquals(-BigDecimal.TEN)
        )
        assertTrue(
            "'-10.' should be -10",
            "-10.".toBigDecimalOrZero().compareEquals(-BigDecimal.TEN)
        )
        assertTrue(
            "'-10.00' should be -10",
            "-10.00".toBigDecimalOrZero().compareEquals(-BigDecimal.TEN)
        )

    }
}

fun BigDecimal.compareEquals(value: BigDecimal) = this.compareTo(value) == 0